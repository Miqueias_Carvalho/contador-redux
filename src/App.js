import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import { addNum, subNum } from "./store/modules/counter/actions";

function App() {
  const num = useSelector((state) => state.counter);
  const dispach = useDispatch();

  const add = () => {
    dispach(addNum(1));
  };

  const sub = () => {
    dispach(subNum(1));
  };

  return (
    <div className="App">
      <header className="App-header">
        <p>{num}</p>
        <div>
          <button onClick={sub}>-</button>
          <button onClick={add}>+</button>
        </div>
      </header>
    </div>
  );
}

export default App;
