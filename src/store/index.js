import { createStore, combineReducers } from "redux";

//importando reducers
import reducerCounter from "./modules/counter/reducer";

//combinando todos os reducers importados
const reducers = combineReducers({ counter: reducerCounter });

//passando a combinação para a nova store
const store = createStore(reducers);

export default store;
