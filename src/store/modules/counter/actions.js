import { ADD_NUM, SUB_NUM } from "./actionTypes";

export const addNum = (num) => ({
  type: ADD_NUM,
  num: num,
});

export const subNum = (num) => ({
  type: SUB_NUM,
  num: num,
});
