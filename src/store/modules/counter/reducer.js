import { ADD_NUM, SUB_NUM } from "./actionTypes";

const reducerCounter = (state = 0, action) => {
  switch (action.type) {
    case ADD_NUM:
      return state + action.num;
    case SUB_NUM:
      return state - action.num;
    default:
      return state;
  }
};
export default reducerCounter;
